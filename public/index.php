<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('web_server', 'dev');  // 'dev' or 'prod' 

/**
 *
 * Методы для подключения файлов
 * @read http://php.net/manual/ru/function.require.php
 */
require(__DIR__ . '/../config/_bootstrap.php');
$config = require(__DIR__ . '/../config/config.php');



try {
    $application = new \core\Application($config);
    $result = $application->run();
} catch (Exception $e){
    if (web_server=='dev') { echo "Error with start application: {$e->getMessage()}" ;
    } else {
        require_once '404.php';
    }
}



