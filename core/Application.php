<?php

namespace core;

use core\components\Request;
use core\components\Validateconfig;
use core\exceptions\InvalidApplicationConfig;
use core\exceptions\InvalidApplicationController;



class Application
{
    public $id;

    public $baseDir;

    public $controllersNamespace;

    public $baseController;
    
    public $errorAction;

    protected $request;

    /**
     * @param array $config
    */
    public function __construct(array $config = [])
    {
        $this->validateConfig($config);

        foreach ($config as $key => $param) {
            $this->{$key} = $param;
        }
        $this->request = new Request();
    }


    protected function validateConfig(array $config)
    {
        /**
         * @TODO  DZ
         * Валидация конфига только в версии разработки
         * Реализовать валидацию по паттерну Strategy
         */
        
        $vconfig = new Validateconfig($config);
        $vconfig = $vconfig->getConfig();

        if ($vconfig == 'ok') {
            return array($config);
        } else {
            throw new InvalidApplicationConfig($vconfig);
            exit();
        }
    }

    /**
     * @return integer
    */
    public function run()
    {
        /**
         * @TODO  DZ
         * При вызове неизвестного контроллера вызываться страницу с ошибкой
         * При вызове неизвестного действия в котроллере вызываем страницу с ошибкой
         */
        $defaultController = $this->request->hasQueryString() ? null : $this->baseController;
        $controllerNamespace = $this->request->getControllerNamespace($defaultController);
        $controllerNamespace2 = $this->request->getControllerNamespace($defaultController);
        $controllerNamespace = $this->controllersNamespace . $controllerNamespace . 'Controller';
        if(class_exists($controllerNamespace)){
            /** @var $controller \core\base\WebController */
            $controller = new $controllerNamespace(); //&&($defaultController!=NULL)
//if ((method_exists($controllerNamespace2, "action".ucfirst($this->request->getControllerAction($defaultController))))) {
            $controller->runAction($this->request->getControllerAction($defaultController));
           //echo $this->request->getControllerAction($defaultController);
  //           } else {
  /*???? не выводится default !!!*/  // throw new exceptions\InvalidApplicationAction('This action not exist'); 
    //      }
        } else {
            //$this->errorActionLaunch();
            throw new exceptions\InvalidApplicationController('This controller not exist');
        }
        return 1;
    }
    
    protected function errorActionLaunch()
    {
        $arr = explode('/', ltrim($this->errorAction, "/"));
        $controller = "\\controllers\\".ucfirst($arr[0])."Controller";
        $action = "action".ucfirst($arr[1]);
        $controller = new $controller;
        return $controller->{$action}();
    }
}