<?php
/**
 * Created by PhpStorm.
 * User: pudchdenis
 * Date: 01.04.17
 * Time: 17:33
 */

namespace core\base;
use core\exceptions\InvalidApplicationAction;

class WebController
{
    public $defaultAction = 'index';
    
    public function runAction($name)
    {
        /*
         * @TODO DZ
         *  обработать ошибку вызова не существующего действия
         *  с помощью создания исключения (Exception)
        */
        if(empty($name)){
            $this->{'action'.ucfirst($this->defaultAction)}();
        }else{
            if (method_exists($this, 'action'.ucfirst($name))) {
            $this->{'action'.ucfirst($name)}();
            } else {
                throw new InvalidApplicationAction('This action not exist'); 
            }
        }
    }
}