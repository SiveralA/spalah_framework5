<?php

namespace core\components;


interface configValidateStrategy {
    public function configValidate ($config);
}

class validateInDev implements configValidateStrategy {
    public $configError;
    public function configValidate($config) {
      if (!empty($config)) {
        foreach ($config as $key => $value) {
            if (empty($config[$key])) {
                $this->configError = "One argument in config (".$config[$key].") is empty. ";
            }
        }
        
        (!is_string($config['id'])) ? $this->configError .= "Value [id] is not a string. " : '';
//не получилось (!is_dir($config['baseDir'])) ? $this->configError .= "Value [baseDir] is not a dir.  - ".$config['baseDir'] : '';
//не получилось (!is_dir($config['controllersNamespace'])) ? $this->configError .= "Value [controllersNamespace] is not a dir. " : '';
        (!class_exists("controllers\\" . ucfirst($config['baseController'])."Controller")) ? $this->configError .= "Value [baseController] is not a controller. " : '';
        
        if (!empty($this->configError)) {
            return $this->configError;
        } else {
            return "ok";
        }
     } else {
        $this->configError = 'All config is empty. ';
        return $this->configError;
    }
}
}

//class validateInProd implements configValidateStrategy {
//    public function configValidate($config) {
//        return array($config);
//    }
//}

class Validateconfig {
    
    public $configChangeTime;
    public $lastValidateTime;
    public function __construct($config) {
        $this->config = $config;
        $this->lastValidateTime = file_get_contents("../config/config.chtime");
        $this->configChangeTime = date ("Y-m-d H:i:s", filemtime(__DIR__ . '/../../config/config.php'));
    }

    
    
    public function getConfig () {

        //if ($this->lastValidateTime > $this->configChangeTime) echo "Проверка была позже чем изменение"; else echo "Изменение было позже чем проверка";
        if ($this->lastValidateTime < $this->configChangeTime) {
        
        if (web_server == 'dev') {
            $vconfig = new validateInDev ();
            $vconfig = $vconfig->configValidate($this->config);
        }
        elseif (web_server == 'prod') {
            $vconfig = 'ok';
        } else {
            $vconfig = "web_server_is_fail";
        }
        
        if ($vconfig=='ok') {
            $time = date("Y-m-d H:i:s");
            $cache = fopen("../config/config.chtime", "w");
            fwrite($cache, $time);
            fclose($cache);
        }
        return $vconfig;
    } else {
        return "ok";
    }
}
}