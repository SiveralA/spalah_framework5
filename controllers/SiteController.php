<?php
/**
 * Created by PhpStorm.
 * User: pudchdenis
 * Date: 01.04.17
 * Time: 17:08
 */

namespace controllers;

use core\base\WebController;

class SiteController extends WebController
{
    public function actionIndex()
    {
        echo "/* site/index */<br>";
    }
    
    public function actionError()
    {
        echo 'You have error<br>';
    }
}