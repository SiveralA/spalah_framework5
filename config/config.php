<?php

return [
    'id' => 'spalah_framework',
    'baseDir' => __DIR__ . DIRECTORY_SEPARATOR .'spalah_framework',
    'controllersNamespace' => "\\controllers",
    'baseController' => 'site',
    'errorAction' => 'site/error' 
];